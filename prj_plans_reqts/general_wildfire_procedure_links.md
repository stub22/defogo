+ Wildland fire information: https://inciweb.nwcg.gov/

+ ICS information: https://www.usda.gov/sites/default/files/documents/ICS100.pdf

+ Wildland Fire Incident Management Field Guide: https://www.nwcg.gov/publications/210