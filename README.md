# defogo

Defogo is an open-science project for Wildfire Data Analysis, launched in 
December 2018.  

This repository contains in-progress analysis scripts 
and datasets, which may be removed or modified unpredictably.

The contents of this project are shared under terms of a [BSD 3-clause license](LICENSE)

All information and software contained here are incomplete
artifacts of research works-in-progress.  
None of these artifacts are intended to be used for any real-world wildfire
analysis, suppression, or mitigation.  

This work is produced by a northwest US research team which includes staff from:
* [AASO (Autonomous Aerial Systems Office)](https://www.umt.edu/aaso/) at University of Montana, led by Jen F.
  * Creators of the [DroneFire](http://www.umt.edu/aaso/DroneFire/) program.
<!-- 
* [Missoula Technology and Development Center (MTDC)](https://www.fs.fed.us/eng/techdev/mtdc.htm) 
  * Field requirements, testing, usability analysis
-->
* GIS and earth systems analysts Kiksapa, led by Pat. O.
  * Wildfire risk/spread models:  topography, fuel, hazard, weather 
* AI systems developer [Appstract](http://www.appstract.com) led by [Stu B22](https://gitlab.com/stub22).
  * Knowledge model, application pipeline

Outer Context Diag:

![defogo_context_03E](web_img/defogo_context_04H.png)

Mission
-------
* Defogo WUI Analyzer : 2018Q4-present 
  * Processing of image and sensor data collected from DroneFire
  * Photogrammetry and integration with GIS data
  * Image analysis to identify wildfire hazards and fuels
  * Integration with weather data and forecasts
  * Estimates of WUI hazard level 
* Defogo WRF-Fire binding : Planned to begin 2020Q1

Background
----------

+ Geospatial data standards:  http://www.opengeospatial.org/

+ Landfire (fire hazard modeling): https://www.landfire.gov/

+ NASA & USFS-RSAC fire imaging overview 2016 : https://go.nasa.gov/2RQB4e2

+ NIROPS =  National Infrared Operations : https://fsapps.nwcg.gov/nirops/

+ Wildland Fire Incident Management Field Guide : https://www.nwcg.gov/publications/210

+ WUI Hazard scale, 2012:  https://www.fs.usda.gov/pnw/publications/framework-addressing-national-wildland-urban-interface-fire-problem%E2%80%94determining-fire

+ WRF-SFIRE user guide : http://www.openwfm.org/wiki/WRF-SFIRE_user_guide 


Platforms 
-----------

+ Field Station
  + Native field apps deployed to field devices primarily running Linux or a RTOS.
  + Developed using C/C++ and Rust for efficient concurrency, safety, and security
  
+ Field User Web UI
  + Two UI formats (same Web code)
    + Display kiosk shared by team e.g. monitor connected to a Field Station
    + Handheld per mobile user 
  + All formats can be served from local Field Station web server, department server, and/or cloud services
  + UI implemented using HTML + WebComponents + Javascript + JSON 

+ Cloud Services
  + Deploy in commercial or private cloud such as AWS, Google cloud 
 
+ Desktop + Lab Analysis, Reporting, Authoring
  + Open or commercial, e.g.:  R, SciPy, ArcGIS, MatLab

Ingredients
-----------
+ Online GIS:  OSGeo - https://www.osgeo.org/  
  + quick-start here:   https://live.osgeo.org/en/overview/overview.html
+ Online image processing: Map-TK (Motion-imagery Aerial Photogrammetry Toolkit)   
  + https://github.com/Kitware/maptk
+ Offline server prep:  Ontology models, modal logic and proof systems
  + Related to "Glue.ai Kernel" = http://www.glue.ai/glue_kernel.html

Components
------------
Each of these symbolic "dfg___" comps maps to a (family of) system runtime boundary(-ies). 

### Field Station Components
#### Prc - Processor
+ dfgprc - Prc = embedded processor runtime:  native app compiled from Rust + C, tgt linux + MS-Win
  + Processor implements all heavy numerical processing, 
  + Reads input streams from field device ports, writes outputs, uses Dsd storage resource. 
  + Configured by docs output from dfgkmo Kmp compiler output.
+ dfgpri - Pri = sensor input stream - video, images, audio, block or serial, MIDI
+ dfgpro - Pro = effector output stream - same formats as dfgpri, out for HDMI or Web-Video
  + Often host machine is configured with some Pro HD streams routed straight to device HDMI out  
#### Dsd - Storage
+ dfgdsd - Dsd = Data storage daemon 
  + Manages all data persistence, filesystems, reads, writes, queries
  + Responsible for Defogo Base field GIS data setup and cloud sync
+ dfgpdr - Pdr = processor data storage read stream - data flow in from Dsd to Prc
+ dfgpdw - Pdw = processor data storage write stream - data flow out from Prc to Dsd

### User Interaction Components
+ dfgwcl - Wcl = web client runtime: HTML5 + Typescript, Scala-JS, JSON 

### Lab Author components
+ dfgkmp - Kmp = compiler runtime:  Scala, Akka, with correctness proofs via Agda, MMT.
+ dfgkmi - Kmi = compiler intput:  RDF, optional OWL, SPARQL, SHACL, Rule-ML, RETE, prolog (see glue.ai by stub22 et al.)
+ dfgkmo - Kmo = compiler output:  Prc config doc (XML, JSON, SQL...)  + Wcl web UI HTML+JS

Function Pipeline Example
---------------

![defogo_pipeImgToWUI](web_img/defogo_pipeline_04L.png)
